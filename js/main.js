$(document).ready(function(){
	$('.dropdown-toggle').dropdown();
	footerPosition();
});

$(window).resize(function(){
	footerPosition();
});

function footerPosition()
{
	if(window.innerHeight >= $('body').height())
		$('.footer').css('margin-top', window.innerHeight - $('body').height() + 'px');
}