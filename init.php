<?php
	define('INDEX_FILE_LOCATION', __FILE__);

	// echo 'intentando leer: ' . dirname(INDEX_FILE_LOCATION) . '/config.ini<br>';
	$fd = @fopen(dirname(INDEX_FILE_LOCATION) . '/config.ini', "r");
	$extra_baseURL = fgets($fd, 1024);
	$extra_baseDIR = fgets($fd, 1024);
	fclose($fd);

	$extra_baseURL = preg_replace('/\r\n|\r|\n/', '', $extra_baseURL);
	$extra_baseDIR = preg_replace('/\r\n|\r|\n/', '', $extra_baseDIR);

	$baseURL 	= "http://" . $_SERVER['HTTP_HOST'] . $extra_baseURL . '/fundamentos/';
	$head 		= dirname(INDEX_FILE_LOCATION) . '/templates/common/head.php';
	$header 	= dirname(INDEX_FILE_LOCATION) . '/templates/common/header.php';
	$footer 	= dirname(INDEX_FILE_LOCATION) . '/templates/common/footer.php';
	$baseDIR 	= dirname(INDEX_FILE_LOCATION);
?>