<?php
	require('./init.php');
?>
<!DOCTYPE HTML>
<html lang="es">
	<?php include $head; ?>
	<body>
		<?php include $header; ?>

		<div class="row-fluid mainRow">
			<div class="carousel slide">
				<!-- Carousel items -->
				<div class="carousel-inner mycarousel">
					<div class="active item mycarousel-item">
						<div class="mycarousel-item-legend">Conoce nuestros programas<br>de diplomados y postitulos</div>
						<img class="mycarousel-image" src="<?php echo $baseURL; ?>img/usach01.jpg" style="top: -100px">
					</div>
					<div class="item mycarousel-item">
						<div class="mycarousel-item-legend">Perfecciona tus habilidades<br>y amplia tus horizontes</div>
						<img class="mycarousel-image" src="<?php echo $baseURL; ?>img/usach02.jpg" style="top: -250px">
					</div>
					<div class="item mycarousel-item">
						<div class="mycarousel-item-legend">En una de esas a tu lado<br>se sienta una mina rica</div>
						<img class="mycarousel-image" src="<?php echo $baseURL; ?>img/usach03.jpg" style="top: -300px;">
					</div>
				</div>

				<!-- Carousel items -->
				<div id="move-left" class="carousel-control left" data-slide"prev">&lsaquo;</div>
				<div id="move-right" class="carousel-control right" data-slide"next">&rsaquo;</div>
			</div>
		</div>

		<script type="text/javascript">
			$('.carousel').carousel({ interval: 5000 });	// Crea carousel con 5 seg de intervalo
			$('#move-left').click(function(){
				$('.carousel').carousel('prev');			// Retrocede
			});
			$('#move-right').click(function(){				
				$('.carousel').carousel('next');			// Avanza
			});
		</script>

		<?php include $footer; ?>
	</body>
</html>