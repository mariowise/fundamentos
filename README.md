### Comenzando con git

Para comenzar a trabajar con git es necesario seguir los siguientes pasos

Ir a una carpeta o crear una donde se pondra el repo

Iniciar el trabajo con git SVN

	git init

Vincular con le origen

	git remote add origin git@bitbucket.org:mariowise/fundamentos.git

Clonar el repositorio o descargarlo a tu PC local

	git clone git@bitbucket.org:mariowise/fundamentos.git


### Trabajo cotidiano de git

En el trabajo cotidiano de git debemos considerar

Realizar un pull del repositorio siempre antes de comenzar a trabajar

	git pull

Al haber realizado cambios compactarlos, añadirles una etiqueta y enviarlos a la nube

	git add *				Agrega archivos creados y modificados (git add -u agrega solo los que sufrieron modificaciones)
	git commit -m '<Comentario>'		
	git push 				Envia todo a la nube

De ocurrir un error en el punto 2 es necesario volver a hacer un pull pues al parecer algun compañero esta trabando tambien y ha hecho cambios


### Herramienta de merge

Es OBLIGAGORIO tener un programa para solucionar los merge-problem

	sudo apt-get install meld

En caso de sufrir un conflicto de merge irreparable, llamar la herramienta de merge para solucionar a mano el conflicto

	git mergetool

Normalmente al realizar el merge a mano, se generan archivos .orig que es recomendable eliminar

	rm -rf *.orig



### Informacion adicional

Adicionalmente git puede ser instalado en un pc desde cero con el siguiente comando

	sudo apt-get install git-core

Luego sera necesario crear una llave SSH con el comando ojala en la ruta por defecto /home/mario/.ssh/id_rsa

	ssh-keygen

Que creara na fingerprint que debera se ingresada a bitbucket.org en la seccion MANAGE ACCOUNT/SSH KEYS es necesario ingresar el contenido del siguiente archivo

	/home/mario/.ssh/id_rsa.pub




Algunas veces se han creado archivos en el repositorio y se han eliminado del local, pero al realizar el proceso de git add * no los elimina del repositorio, esto es porque a git hay que explicitarle que los eline de la nube

	git rm $(git ls-files --deleted)	Elimina los archivos eliminados en el local, en el repositorio


