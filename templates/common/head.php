<head>
	<title>Sistema diplomados</title>
	<meta charset="utf-8"/>
	<meta name="author" content="Mario Lopez - Fabian Arismendi - Jose Orellana">

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<!-[if lt IE 9]> <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script> <!-[endif]->
	<link href="<?php echo $baseURL; ?>templates/common/bootstrap/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo $baseURL; ?>templates/common/bootstrap/css/bootstrap-responsive.css" rel="stylesheet" media="all">
	
	<link href="<?php echo $baseURL; ?>styles/main.css" rel="stylesheet">

	<!-- css para fuentes -->
	<link href='http://fonts.googleapis.com/css?family=Share+Tech' rel='stylesheet' type='text/css'>
	
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo $baseURL; ?>templates/common/bootstrap/js/bootstrap.js"></script>
	
	<script type="text/javascript" src="<?php echo $baseURL; ?>js/main.js"></script>
</head>
