<?php
    require_once($baseDIR . '/controllers/login.php');
?>

<div class="navbar navbar-inverse navbar-fixed-top navbarUsach">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <a class="brand" href="<?php echo $baseURL ?>"><span class="dipp">DIPLOMADOS</span>
                <span style="color: white!important;">
                    U<span style="color: rgb(180,180,180)!important;">de</span>Santiago
                </span>
            </a>
            <div class="nav-collapse collapse">
                <ul class="nav menuGrav">
                    <li id="Inicio" class="active"><a href="<?php echo $baseURL; ?>">Inicio</a></li>
                    <li id="Diplomados"><a href="<?php echo $baseURL; ?>templates/diplomados.php">Diplomados</a></li>
                    <li id="Post-titulos"><a href="">Post-titulos</a></li>
                    <li id="Portal"><a href="http://www.udesantiago.cl/">Portal</a></li>
                    
                    <?php if(!check_login()): ?>
                        <!-- USUARIO DESCONOCIDO -->
                        <li class="dropdown" id="menu1">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#menu1">
                                <span>Ingresar</span>
                                <b class="caret"></b>
                            </a>
                            <div class="dropdown-menu pull-right">
                                <form style="margin: 0px" accept-charset="UTF-8"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /><input name="authenticity_token" type="hidden" value="4L/A2ZMYkhTD3IiNDMTuB/fhPRvyCNGEsaZocUUpw40=" /></div>
                                    <fieldset class='textbox' style="padding:10px">
                                        <input id="inputUsername" style="margin-top: 8px" type="text" placeholder="Usuario" />
                                        <input id="inputPassword" style="margin-top: 8px" type="password" placeholder="Contraseña" />
                                        <input class="btn-primary" name="commit" type="button" value="Ingresar" />
                                    </fieldset>
                                </form>
                            </div>
                        </li>
                    <?php else: ?>
                        <!-- USUARIO CONOCIDO -->
                        <li class="dropdown" id="menu1">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#menu1">
                                <span><?php echo $_COOKIE['usr']; ?></span>
                                <b class="caret"></b>
                            </a>
                            <ul id="usermenu" class="dropdown-menu pull-right">
                                <?php if(isset($_COOKIE['alumno'])): ?>
                                    <li><a href="<?php echo $baseURL; ?>templates/debt.php">Deuda</a></li>
                                <?php endif; ?>
                                <li><a id="buttonLogout">Cerrar Sesión</a></li>
                            </ul>
                        </li>
                    <?php endif; ?>

                </ul>
            </div>
            
            <img id="escudo" src="<?php echo $baseURL; ?>img/logoBlanco.png">
            
            <script type="text/javascript">
                $('#escudo').mouseenter(function(){
                    $('#escudo').attr('src','<?php echo $baseURL; ?>img/logoColor.png');
                }).mouseleave(function(){
                    $('#escudo').attr('src','<?php echo $baseURL; ?>img/logoBlanco.png');
                });
                $('.dropdown-toggle').dropdown();
                $('.dropdown-menu').find('form').click(function (e) {
                    e.stopPropagation();
                });
                $('.btn-primary').click(function(){
                    $.ajax({
                        url: '<?php echo $baseURL ?>/controllers/login.php',
                        type: 'POST',
                        data: 'q=login&u=' + $('#inputUsername').val() + '&p=' + $('#inputPassword').val(),
                        success: function(response)
                        {
                            if(response == 'DONE' || response == 'ALUMNO')
                                window.location = '';
                            else
                                alert('Usuario y contraseña incorrectos');
                        }
                    });
                });
                $('#buttonLogout').click(function(){
                    $.ajax({
                        url: '<?php echo $baseURL ?>/controllers/login.php',
                        type: 'POST',
                        data: 'q=logout',
                        success: function(response)
                        {
                            if(response == 'DONE' || response == 'ALUMNO')
                                window.location = '';
                            else
                                alert('Error al cerrar sesion');
                        }
                    });
                });
            </script>
        </div>
    </div>
</div>