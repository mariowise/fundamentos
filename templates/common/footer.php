<div class="footer">
	<div class="container">
		<div class="row-fluid">
			<div class="span4 footer-col1">
				<img src="<?php echo $baseURL; ?>/img/logoDiinf.png">
			</div>
			<div class="span4 footer-col2">
				Sitio web diseñado y contruido por:<br>
				Mario López<br>
				Fabián Arismendi<br>
				José Orellana<br>
			</div>
			<div class="span4 footer-col3">
				Este sitio ha sido diseñado para cumplir las especificaciones del proyecto de 
				bases de datos relacionales. El objetivo es implementar las consultas SQL, las 
				tecnologías de servidor y realizar una aplicación web que permita consultar
				la base de datos.
			</div>
		</div>
	</div>
</div>