<?php
	require_once('../init.php');
?>
<!DOCTYPE HTML>
<html lang="es">
	<?php require($head); ?>
	<body>
		<?php require($header); ?>

		<script type="text/javascript">
			$('#escudo').css('display', 'none');
			$('#Inicio').attr('class', '');
			$('#Diplomados').attr('class', 'active');
		</script>
		
		<div class="container title">
			<div class="pageTitle" style="background-image: url('<?php echo $baseURL; ?>img/diplomados.jpg');">
				<div class="pageTitleContainer">
					<div class="pageTitleMain">
						<span class="pageTitleText">Diplomados</span><br>
						<span class="pageTitleSubText">Informarte sobre las fechas de inscripción</span>
					</div>
				</div>
			</div>
		</div>

		<script type="text/javascript">
			var mainList;
			$.ajax({
				url: '<?php echo $baseURL; ?>controllers/programas.php',
				type: 'POST',
				data: 'q=programas&t=diplomado',
				success: function(response)
				{
					mainList = jQuery.parseJSON(response);
				}
			});
		</script>

		<div class="container" style="margin-top: 10px; margin-bottom: 10px">
			<div class="row-fluid">
				<div class="span2">
					<ul id="facuList" class="nav nav-tabs nav-stacked">
						
					</ul>
				</div>
				<div class="span2">
					<ul id="cursoList" class="nav nav-tabs nav-stacked">
						
					</ul>
				</div>
				<div class="span8">
					<div id="cursoBoard" class="programaBoard">
						Selecciona una una facultad<br> y luego un diplomado
					</div>
				</div>
			</div>
		</div>

		<script type="text/javascript">
			var lista_facultades;
			var facultad_selected;
			var lista_programas;
			$.ajax({
				url: '<?php echo $baseURL; ?>controllers/programas.php',
				type: 'POST',
				data: 'q=facultades',
				async: false,
				success: function(response)
				{
					lista_facultades = jQuery.parseJSON(response);
				}
			});
			for(var i = 0 ; i < lista_facultades.length-1; i++)
				$('#facuList').append('<li><a id="' + lista_facultades[i]['CODIGO_FACULTAD'] + '" onclick="selectFacultad(this)">' + lista_facultades[i]['NOMBRE_FACULTAD'] + '</a></li>');

			function selectFacultad(input)
			{
				unselectList('#facuList');
				facultad_selected = parseInt($(input).attr('id'));
				$(input).css('background-color', '#DDDDDD');
				$.ajax({
					url: '<?php echo $baseURL; ?>controllers/programas.php',
					type: 'POST',
					data: 'q=programas&t=diplomado&f=' + facultad_selected,
					async: false,
					success: function(response)
					{
						lista_programas = jQuery.parseJSON(response);
					}
				});
				if(lista_programas != 'false')
				{
					$('#cursoList').html('');
					for(var i = 0 ; i < lista_programas.length-1 ; i++)
						$('#cursoList').append('<li><a id="'+ i +'" onclick="selectCurso(this)">'+ lista_programas[i]['NOMBRE_CURSO'] +'</a></li>');
				}
				else
				{
					alert('No se han encontrado programas de esa facultad');
					$('#cursoList').html('');
					unselectList('#facuList');
				}
			}

			function unselectList(list)
			{
				var aux;
				for(var i = 0 ; i < $(list).children().length ; i++)
				{
					aux = $(list).children()[i];
					$(aux).children().css('background-color', 'white');
				}
				$('#cursoBoard').html('');
				$('#cursoBoard').attr('class', 'programaBoard');
				$('#cursoBoard').html('Selecciona una una facultad<br> y luego un diplomado');
			}

			function selectCurso(curso)
			{
				var programa = parseInt($(curso).attr('id'));
				var row = lista_programas[programa];
				$('#cursoBoard').html('');
				$('#cursoBoard').attr('class', 'programaBoardFound');
				$('#cursoBoard').html(
					'<h1>'+row['NOMBRE_CURSO']+'</h1><br>'+
					'<h3>Descripción:</h3><br>'+
					'<span>'+row['DESCRIPCION']+'</span><br>'+
					'<h3>Director:</h3><br>'+
					'<span>'+row['DIRECTOR']+'</span><br>'+
					'<h3>Profesor guía:</h3><br>'+
					'<span>'+row['PROFESOR_GUIA']+'</span><br>'+
					'<h3>Objetivos:</h3><br>'+
					'<span>'+row['OBJETIVOS']+'</span><br>'
				);
			}

		</script>
		
		<?php require($footer); ?>
	</body>
</html>