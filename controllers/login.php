<?php
	/*
	Para facilitar la prueba mediante URL se hara uso de $_REQUEST en ves de $_POST
	*/
	if(isset($_REQUEST['q']))
		$request = $_REQUEST['q'];											// Obtengo la peticion a login.php
	else
		$request = '';

	// Peticion de login
	if($request == 'login')													// Si me piden login
	{
		$user = $_REQUEST['u'];												// Obtengo el username
		$pass = $_REQUEST['p'];												// Obtengo la password
		if($user != '' && $pass != '')										// Si los datos no vienen blancos
		{
			require('dbconnect.php');										// Conecto con la base de datos
			$pass = md5($pass);												// Aplico ecriptacion a la contraseña
			$query = mysql_query(											// Envio de la consulta SQL
				"SELECT * FROM USUARIO WHERE NOMBRE_PERSONA = '$user'"		// Selecciono los usuarios con el mismo nombre
			);
			if(mysql_num_rows($query) == 1)									// Si encuentro el usuario en cuestion (unico)
			{
				$tupla = mysql_fetch_assoc($query); 						// Construyo un arreglo asosiativo de la tupla donde la llave es el nombre de atributo
				if($tupla['CONTRASENA'] == $pass)							// Si las contraseñas son iguales (md5 es una funcion biyectiva)
				{
					setcookie('usr', $user, time() + 3600*24, '/'); 		// Registro la cookie de usuario
					setcookie('pass', $pass, time() + 3600*24, '/');		// Registro la cookie de contraseña
					echo 'DONE';											// Envio una respuesta de login realizado
				}
				else 														// Contraseña equivocada para un username valido
					echo 'WRONG';											// Envio una respuesta de contraseña mala
			} 
			else 															// Si no ha encontrado la tupla
			{
				$query = mysql_query(
					"SELECT * FROM ALUMNO WHERE RUT_PERSONA = '$user'"
				);
				if(mysql_num_rows($query) == 1)
				{
					setcookie('usr', $user, time() + 3600*24, '/');
					setcookie('pass', 'alumno', time() + 3600*24, '/');
					setcookie('alumno', 'true', time() + 3600*24, '/');
					echo 'ALUMNO';
				}
				else
				{
					echo 'NULL';
				}
			}
			mysql_close();
		}
	}

	// Peticion de logout
	if($request == 'logout')
	{
		logout();
		echo 'DONE';
	}

	// funcion para se incluida en el logout
	function logout()	
	{
		$user = $_COOKIE['usr'];
		$pass = $_COOKIE['pass'];
		setcookie('usr', $user, time() - 3600*24, '/');
		setcookie('pass', $pass, time() - 3600*24, '/');
		if(isset($_COOKIE['alumno'])) setcookie('alumno', '', time() - 3600*24, '/');
	}

	// revista si estan seteadas las cookies de usuario
	function check_login()
	{
		if(isset($_COOKIE['usr']) && isset($_COOKIE['pass']))
			return true;
		else
			return false;
	}
?>