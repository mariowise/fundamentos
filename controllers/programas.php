<?php

	if(isset($_REQUEST['q']))
		$req = $_REQUEST['q'];
	else
		$req = '';

	switch ($req) {
		case 'programas':													// Se estan solicitando los programas
			$array = getProgramas(											// Se envia la query
				(isset($_REQUEST['t'])) ? $_REQUEST['t'] : '', 				// En caso de que se especifique un tipo
				(isset($_REQUEST['f'])) ? $_REQUEST['f'] : ''				// En caso de que se especifique un filtro
			);
			echo json_encode($array);										// Se entrega el resultado en formato JSON para compatibilidad con Javascript
			break;
		case 'facultades':
			$array = getFacultades();
			echo json_encode($array);
			break;
	}
	
	function getProgramas($tipo = '', $facu = '')
	{
		include('dbconnect.php');
		$query = "SELECT * FROM FACULTAD JOIN CURSO JOIN PROGRAMA WHERE CURSO.ID_PROGRAMA = PROGRAMA.ID_PROGRAMA AND FACULTAD.CODIGO_FACULTAD = CURSO.CODIGO_FACULTAD AND CURSO.activo = '1'";
		if($tipo != '') $query = $query . " AND CURSO.TIPO = '" . $tipo . "'";
		if($facu != '') $query = $query . " AND FACULTAD.CODIGO_FACULTAD = '" . $facu . "'";
		$query = mysql_query($query);												// Envio de la query
		mysql_close();
		$result = array();
		$i = 0;
		while($result[$i] = mysql_fetch_assoc($query))								// Construyo un arreglo de arreglos asociativos
			$i++;																	
		return $result;																// Retorno el arreglo de tuplas fetchadas
	}

	function getFacultades()
	{
		include('dbconnect.php');
		$query = "SELECT * FROM FACULTAD";
		$query = mysql_query($query);
		mysql_close();
		$result = array();
		$i = 0;
		while($result[$i] = mysql_fetch_assoc($query))
			$i++;
		return $result;
	}
	
?>